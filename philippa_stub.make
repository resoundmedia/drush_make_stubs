; Core
core = 6.x
api = 2

projects[pressflow][type] = "core"
projects[pressflow][download][type] = "get"
projects[pressflow][download][url] = "http://files.pressflow.org/pressflow-6-current.tar.gz"
projects[pressflow][patch][] = "http://drupal.org/files/issues/555362-1.update_parse_files.D6_0.patch"

; Check out philippa's profile
projects[philippa][type] = "profile"
projects[philippa][download][type] = "git"
projects[philippa][download][url] = "git@bitbucket.org:resoundmedia/philippa.git"
