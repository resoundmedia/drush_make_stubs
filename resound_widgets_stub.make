core = 7.x
api = 2

projects[drupal][type] = core
projects[drupal][version] = "7.14"
; A fix for file fields upload. Manifested itself in field_collection.
; See http://drupal.org/node/1329856 and http://drupal.org/node/1468686#comment-5859258
projects[drupal][patch][] = "http://drupal.org/files/drupal-1468686-8.patch"
; We don't want to be appearing in search results.
projects[drupal][patch][] = "https://bitbucket.org/resoundmedia/patches/raw/04ff1ce13b62/drupal_disallow_search_engines.patch"

projects[resound_widgets][type] = profile
projects[resound_widgets][download][type] = git
projects[resound_widgets][download][url] = git@bitbucket.org:resoundmedia/resound_widgets.git
