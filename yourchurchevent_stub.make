core = 7.x
api = 2

projects[drupal][type] = "core"
projects[drupal][version] = "7.15"

projects[yourchurchevent][type] = "profile"
projects[yourchurchevent][download][type] = "git"
projects[yourchurchevent][download][url] = "git@bitbucket.org:resoundmedia/yourchurchevent.git"
projects[yourchurchevent][download][branch] = "drupal"
