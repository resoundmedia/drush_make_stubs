; Core
core = 6.x
api = 2

projects[pressflow][type] = "core"
projects[pressflow][download][type] = "get"
projects[pressflow][download][url] = "http://files.pressflow.org/pressflow-6-current.tar.gz"

; Check out the profile
projects[spokengroove][type] = "profile"
projects[spokengroove][download][type] = "git"
projects[spokengroove][download][url] = "git@bitbucket.org:resoundmedia/spokengroove.git"
